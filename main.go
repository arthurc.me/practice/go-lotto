package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

func input(ask string) (result string) {
	fmt.Print(ask)
	reader := bufio.NewReader(os.Stdin)
	result, err := reader.ReadString('\n')

	if err != nil {
		log.Fatalln(err)
	}

	result = strings.Replace(result, "\n", "", -1)
	return
}

func numberChoice() []int {
	var result []int
	for i := 1; i <= 49; i++ {
		result = append(result, i)
	}

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 7; i++ {
		t := rand.Intn(49 - i)
		result[i], result[t] = result[t], result[i]
	}

	return result[:7]
}

func userChoice() (userInputs []int) {
	index := 1
	for true {
		result, err := strconv.Atoi(input(fmt.Sprintf("輸入第 %d 個數字：", index)))
		if err != nil {
			fmt.Print("這不是數字\n\n")
			continue
		}

		if contain(userInputs, result) {
			fmt.Println("這個數字已經輸入過了")
			continue
		}

		index++
		userInputs = append(userInputs, result)
		if len(userInputs) == 7 {
			break
		}
	}

	return
}

// contain 比對 value 是否在 target 中
func contain(target []int, value int) bool {
	for _, t := range target {
		if t == value {
			return true
		}
	}

	return false
}

// comparison 比對相同的有哪些
func comparison(target []int, choice []int) (result []int) {
	for _, c := range choice {
		if contain(target, c) {
			result = append(result, c)
		}
	}

	return
}

func sliceJoin(target []int, c string, with string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(target), c, with, -1), "[]")
}

func main() {
	var choice []int
	if input("請問要自行選號嗎？ [Y/n] ") != "n" {
		choice = userChoice()
	} else {
		choice = numberChoice()
	}

	target := numberChoice()
	result := comparison(target, choice)

	sort.Ints(target)
	sort.Ints(result)

	fmt.Println("開獎號碼為：" + sliceJoin(target, " ", ", "))
	fmt.Println("選擇的號碼為：" + sliceJoin(choice, " ", ", "))
	fmt.Printf("您一共命中 %d 個數字\n", len(result))
	if len(result) != 0 {
		fmt.Println("命中的號碼為：" + sliceJoin(result, " ", ", "))
	}
}
